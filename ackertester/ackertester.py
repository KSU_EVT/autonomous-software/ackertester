import rclpy
import sys
from rclpy.node import Node
from math import sin

from ackermann_msgs.msg._ackermann_drive_stamped import AckermannDriveStamped

class Ackertester(Node):
    def __init__(self):
        super().__init__('ackertester')
        self.publisher_ = self.create_publisher(AckermannDriveStamped, 'ackertester', 10)
        self.timer_ = self.create_timer(timer_period_sec=0.01, callback=self.timer_callback)

        self.iteration = 0

    def timer_callback(self):
        msg = AckermannDriveStamped()
        msg.drive.speed = (.5 * sin(.0157 * (self.iteration)) + 1.5)
        msg.drive.steering_angle = (.4 * sin(.0157 * (self.iteration)))

        self.publisher_.publish(msg)

        self.iteration += 1

def main(args=None):
    rclpy.init(args=args)
    ackertester = Ackertester()
    rclpy.spin(ackertester)
    ackertester.destroy_node()
    rclpy.shutdown()
    sys.exit()

if __name__ == "__main__":
    main()